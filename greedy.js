const readline = require('readline')

let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

rl.on('line', line => {
  console.log(greedy(line))
})

function greedy(number) {
  let amount = Math.ceil(number * 100)
  amount = amount % 100
  let denos = [25, 10, 5, 1]
  let owed = 0
  denos.forEach(deno => {
    if (amount >= deno) {
      owed = owed + Math.floor(amount / deno)
      amount = amount % deno
    }
    if (amount === 0) return owed
  })
  return owed
}
