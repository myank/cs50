#include <stdio.h>
// #include <math.h>

int main(void){
    float amount;
    int change;
    int owed = 0;
    int deno[] = {25, 10, 5, 1};

    scanf("%f", &amount);
    change = (int)(amount * 100);
    change = change % 100;
    for(int i = 0; i < 4; i++){
        if(change >= deno[i]){
            owed = owed + change / deno[i];
            change = change % deno[i];
        };
    };
    printf("%d \n", owed);
}
