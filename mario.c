#include <stdio.h>

int main(void){
int n;
 scanf("%d", &n);
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n + 1; j++){
           /* each row consists of n - 2 + i no of spaces where n is total number of rows and i is the row number. Rest of the
            * characters are # */ 
            if(j <= (n - (2 + i))){
                    printf(" ");
                }else{
                    printf("#");
                }
        }
        printf("\n");
    }
}
